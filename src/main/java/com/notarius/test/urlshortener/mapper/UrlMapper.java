package com.notarius.test.urlshortener.mapper;

import com.notarius.test.urlshortener.entity.DbUrl;
import com.notarius.test.urlshortener.model.Url;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel="spring")
public interface UrlMapper {

    Url DbUrlToUrl(DbUrl dbUrl);
    DbUrl UrlToDbUrl(Url Url);
}
