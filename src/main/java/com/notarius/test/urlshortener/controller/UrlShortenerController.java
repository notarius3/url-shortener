package com.notarius.test.urlshortener.controller;

import com.notarius.test.urlshortener.model.ErrorResponse;
import com.notarius.test.urlshortener.model.Url;
import com.notarius.test.urlshortener.service.ShortUrlService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class UrlShortenerController {

    private static Logger logger = Logger.getLogger(UrlShortenerController.class);

    @Autowired
    ShortUrlService shortUrlService;

    @PostMapping("/short-url")
    public ResponseEntity<?> getShortUrl(@RequestBody Url url) {
        try {
            Url shortUrl = shortUrlService.getShortUrl(url);
            return new ResponseEntity<>(shortUrl, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Exception caught in getShortUrl", e);
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/original-url")
    public ResponseEntity<?> getOriginalUrl(@RequestBody Url url) {
        try {
            Url originalUrl = shortUrlService.getOriginalUrl(url);
            if (originalUrl != null) {
                return new ResponseEntity<>(originalUrl, HttpStatus.OK);
            } else {
                throw new IllegalArgumentException("Short url " + url.getShortUrl() + " not found.");
            }
        } catch (Exception exception) {
            logger.error("Exception caught in getOriginalUrl", exception);
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrorMessage(exception.getMessage());
            return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{shortUrlPath}")
    public void redirectToOriginalUrl(@PathVariable String shortUrlPath, HttpServletResponse response) throws IOException {
        logger.info("Redirect " + shortUrlPath + " to original url");
        Url url = new Url();
        url.setShortUrl(shortUrlService.getRootUrl() + shortUrlPath);
        Url originalUrl = shortUrlService.getOriginalUrl(url);
        response.sendRedirect(originalUrl.getOriginalUrl());
    }

    @GetMapping("/urls")
    public ResponseEntity<?> getAllUrls() {
        return new ResponseEntity<>(shortUrlService.getAllUrls(), HttpStatus.OK);
    }

    @DeleteMapping("/{shortUrlPath}")
    public ResponseEntity deleteUrl(@PathVariable String shortUrlPath) {
        shortUrlService.deleteUrl(shortUrlService.getRootUrl() + shortUrlPath);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
