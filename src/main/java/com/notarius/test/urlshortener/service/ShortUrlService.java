package com.notarius.test.urlshortener.service;

import com.google.common.base.Preconditions;
import com.notarius.test.urlshortener.entity.DbUrl;
import com.notarius.test.urlshortener.mapper.UrlMapper;
import com.notarius.test.urlshortener.model.Url;
import com.notarius.test.urlshortener.repository.UrlRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigInteger;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Transactional
@Service
public class ShortUrlService {

    private static Logger logger = Logger.getLogger(ShortUrlService.class);
    public static Charset encodingType = StandardCharsets.UTF_8;

    @Autowired
    UrlRepository urlRepository;

    @Autowired
    UrlMapper urlMapper;

    @Value("${server.port}")
    String port;

    @Value("${server.host.address}")
    String serverHostAddress;

    @Value("${url.max-length}")
    int maxLength;

    @Transactional
    public Url getShortUrl(Url url) throws IOException {
        logger.info("Getting Short Url");
        Preconditions.checkArgument(StringUtils.isNotEmpty(url.getOriginalUrl()), "Url null or empty.");
        String normalizedUrl = getNormalizedUrl(url.getOriginalUrl());
        Preconditions.checkArgument(isUrlValid(normalizedUrl), "Invalid url provided.");
        Url existingUrl = urlRepository.findByOriginalUrl(normalizedUrl).map(dbUrl -> urlMapper.DbUrlToUrl(dbUrl)).orElse(null);
        if (existingUrl == null) {
            url.setOriginalUrl(normalizedUrl);
            String shortUrl = createShortUrl(url.getOriginalUrl());
            DbUrl dbUrl = urlMapper.UrlToDbUrl(url);
            dbUrl.setShortUrl(shortUrl);
            existingUrl = urlMapper.DbUrlToUrl(urlRepository.save(dbUrl));
        }
        logger.info("Getting Short Url completed");
        return existingUrl;
    }

    private boolean isUrlValid(String originalUrl) throws IOException {
        logger.info("Checking Url is valid");
        URL url = new URL(originalUrl);
        HttpURLConnection huc = (HttpURLConnection) url.openConnection();
        int responseCode = huc.getResponseCode();
        logger.info("Checking Url completed");
        return responseCode != HttpURLConnection.HTTP_NOT_FOUND;
    }

    public String createShortUrl(String url) throws UnknownHostException {
        logger.info("Generate new short url");
        BigInteger bi = new BigInteger(textToHex(url), 16);
        return getRootUrl() + converter(bi);
    }

    public String converter(BigInteger number) {
        String baseDigits = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder tempVal = new StringBuilder(number.equals(BigInteger.ZERO) ? "0" : "");
        BigInteger mod = BigInteger.ZERO;
        int it = 0;
        BigInteger base = BigInteger.valueOf(62);
        while (!number.equals(BigInteger.ZERO) && it < maxLength) {
            mod = number.mod(base);
            tempVal.insert(0, baseDigits.charAt(mod.intValue()));
            number = number.divide(base);
            it++;
        }
        return tempVal.toString();
    }

    // https://stackoverflow.com/questions/923863/converting-a-string-to-hexadecimal-in-java
    public String textToHex(String text) {
        byte[] buf = null;
        buf = text.getBytes(encodingType);
        char[] HEX_CHARS = "0123456789abcdef".toCharArray();
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i) {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }

    public String getRootUrl() {
        //return "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port + "/";
        return "http://" + serverHostAddress + ":" + port + "/";
    }

    public Url getOriginalUrl(Url url) {
        logger.info("Getting original Url");
        Preconditions.checkArgument(StringUtils.isNotEmpty(url.getShortUrl()), "Url null or empty.");
        return urlRepository.findByShortUrl(url.getShortUrl()).map(dbUrl -> urlMapper.DbUrlToUrl(dbUrl)).orElse(null);
    }

    public String getNormalizedUrl(String url) {
        logger.info("Getting normalized Url");

        boolean isNormalized = false;
        url = url.toLowerCase().trim();
        Set<String> specialChars = new HashSet<>(Arrays.asList("/", ")", "#", "."));
        while(!isNormalized) {
            if (specialChars.contains(url.substring(url.length() - 1)) ) {
                url = url.substring(0, url.length() - 1).trim();
            } else if(url.length() > 3 && url.substring(url.length() - 3).equals("%20")) {
                url = url.substring(0, url.length() - 3).trim();
            } else {
                isNormalized = true;
            }
        }
        return url;
    }

    public Set<Url> getAllUrls() {
        logger.info("Getting all Urls");
        return urlRepository.findAll().stream().map(dburl -> urlMapper.DbUrlToUrl(dburl)).collect(Collectors.toSet());
    }

    public void deleteUrl(String url) {
        logger.info("Deleting Url" + url);
        urlRepository.deleteByShortUrl(url);
    }

}
