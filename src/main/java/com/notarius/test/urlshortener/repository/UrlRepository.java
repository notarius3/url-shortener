package com.notarius.test.urlshortener.repository;

import com.notarius.test.urlshortener.entity.DbUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UrlRepository extends JpaRepository<DbUrl, String> {

    Optional<DbUrl> findByShortUrl(String shortUrl);

    Optional<DbUrl> findByOriginalUrl(String originalUrl);

    List<DbUrl> findAll();

    void deleteByShortUrl(String shortUrl);

}
