package com.notarius.test.urlshortener;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@SpringBootApplication
public class UrlShortenerApplication {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		SpringApplication.run(UrlShortenerApplication.class, args);
	}

}
