package com.notarius.test.urlshortener;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import com.notarius.test.urlshortener.service.ShortUrlService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import java.math.BigInteger;
import java.net.UnknownHostException;


@SpringBootTest
class UrlShortenerApplicationTests {

    @Autowired
    private ShortUrlService shortUrlService;

    private final String notariusUrl = "www.notarius.com";

    @Value("${url.max-length}")
    private int maxChar;

    @Test
    @DisplayName("Should return a short url")
    public void testCreateShortUrl() throws UnknownHostException {
        assertEquals(shortUrlService.getRootUrl() + "dTva7D2j83", shortUrlService.createShortUrl(notariusUrl));
    }

    @Test
    @DisplayName("Should return string with 10 or less chars")
    public void testShortPathLessOrEqual10Chars() throws UnknownHostException {
        BigInteger bi = new BigInteger(shortUrlService.textToHex(notariusUrl), 16);
        assertTrue(shortUrlService.converter(bi).length() <= maxChar);

        bi = new BigInteger(shortUrlService.textToHex("https://aurelia.io/docs/cli/basics#webpack-vs-built-in-bundler"), 16);
        assertTrue(shortUrlService.converter(bi).length() <= maxChar);

        bi = new BigInteger(shortUrlService.textToHex("https://stackoverflow.com/questions/923863/converting-a-string-to-hexadecimal-in-java"), 16);
        assertTrue(shortUrlService.converter(bi).length() <= maxChar);
    }


    @Test
    @DisplayName("Should return normalized url")
    public void testGetNormalizedUrl() throws UnknownHostException {
        assertEquals(shortUrlService.getNormalizedUrl(notariusUrl), notariusUrl);
        assertEquals(shortUrlService.getNormalizedUrl(notariusUrl + "////"), notariusUrl);
        assertEquals(shortUrlService.getNormalizedUrl(notariusUrl + "/  /"), notariusUrl);
        assertEquals(shortUrlService.getNormalizedUrl(notariusUrl + "/  #"), notariusUrl);
        assertEquals(shortUrlService.getNormalizedUrl(notariusUrl + "  #"), notariusUrl);
        assertEquals(shortUrlService.getNormalizedUrl(notariusUrl + "  ."), notariusUrl);
        assertEquals(shortUrlService.getNormalizedUrl(notariusUrl.toUpperCase()), notariusUrl);
    }

}
